<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TabelController extends Controller
{
    public function table(){
        return view('/adminlte/table');
    }

    public function data_table(){
        return view('adminlte.data-tables');
    }
}
