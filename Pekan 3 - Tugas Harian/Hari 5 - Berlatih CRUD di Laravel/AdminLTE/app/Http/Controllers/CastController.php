<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\DB as FacadesDB;



class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }
    
    public function store(Request $request){

        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast')->with('sukses', 'Data cast berhasil disimpan'); //sukses = session
    }

    public function index(){
        $cast = DB::table('cast')->get();
        //dd($cast); untuk debugging. untuk mengecek apakah query berhasil 
        return view('cast.index', compact('cast'));
    }

    public function show($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first(); //mengamibl data pertama 
        return view('cast.show', compact('cast'));
    }

    public function edit($cast_id){
        $cast = DB::table('cast')->where('id', $cast_id)->first(); //mengamibl data pertama 
        return view('cast.edit', compact('cast'));
    }

    public function update($cast_id, Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
                    ->where('id', $cast_id)
                    ->update([
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio']
                    ]);
        return redirect('/cast')->with('sukses','Berhasil update data cast');
    }

    public function destroy($cast_id){
        $query = DB::table('cast')->where('id', $cast_id)->delete();
        return redirect('/cast')->with('sukses', 'Cast berhasil dihapus');


    }
}
