<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route; //jika error ganti yang bawah
//use Illuminate\Routing\Route; // jika error lagi ganti lagi ke yang atas.

Route::get('/', function () {
    return view('welcome');
});

Route::get('/table', 'TabelController@table');
Route::get('/data-tables','TabelController@data_table');

Route::get('/cast/create','CastController@create');

Route::post('/cast','CastController@store'); //route sama sama /cast tpi tidak error karen methodenya beda
Route::get('/cast','CastController@index');

Route::get('/cast/{cast_id}','CastController@show');
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');
Route::delete('/cast/{cast_id}','CastController@destroy');

