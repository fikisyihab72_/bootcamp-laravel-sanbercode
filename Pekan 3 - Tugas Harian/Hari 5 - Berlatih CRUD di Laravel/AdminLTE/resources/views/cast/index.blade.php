@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Cast Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('sukses'))
                <div class="alert alert-success">
                    {{ session('sukses')}}
                </div>
            @endif
            <a class="btn btn-primary mb-2" href="/cast/create">Input Cast Baru</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Nama</th>
                        <th>Umur</th>
                        <th>Bio</th>
                        <th style="width: 40px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($cast as $key => $cast_satuan)
                        <tr>
                            <td> {{ $key + 1}} </td>
                            <td> {{ $cast_satuan->nama}} </td>
                            <td> {{ $cast_satuan->umur}} </td>
                            <td> {{ $cast_satuan->bio}} </td>
                            <td style="display:flex;">
                                <a href="/cast/{{$cast_satuan->id}}" class="btn btn-info btn-sm">Show</a>
                                <a href="/cast/{{$cast_satuan->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                                <form action="/cast/{{$cast_satuan->id}}/" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" align="center">Data kosong</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer clearfix">
            <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">«</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">»</a></li>
            </ul>
        </div> -->
    </div>

</div>


@endsection